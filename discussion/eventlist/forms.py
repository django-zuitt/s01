from django import forms

class AddEventForm(forms.Form):
	event_name = forms.CharField(label = 'event_name', max_length =50)
	description = forms.CharField(label = 'description', max_length = 200)