from django.urls import path
# import the views.py from the same folder
from . import views


app_name = 'eventlist'

urlpatterns = [
	path('event/<int:eventitem_id>/', views.eventitem, name = "vieweventitem"),
	path('index', views.index, name = "index"),
	path('add_event', views.add_event, name = "add_event"),
]