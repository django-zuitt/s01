from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.

from django.http import HttpResponse

from .models import EventItem

from django.contrib.auth.models import User

from django.forms.models import model_to_dict

from .forms import AddEventForm

from django.utils import timezone

def index(request):

	eventitem_list = EventItem.objects.filter(user_id = request.user.id)
	context = {
		'eventitem_list': eventitem_list,
		"user": request.user
	}

	return render(request, "index.html", context)


def eventitem(request, eventitem_id):

	eventitem = get_object_or_404(EventItem, pk = eventitem_id)

	return render(request, "eventitem.html", model_to_dict(eventitem))


def add_event(request):
	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)

		if form.is_valid() == False:
			form = AddEventForm()

		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']

			duplicates = EventItem.objects.filter(event_name = event_name, user_id = request.user.id)

			if not duplicates:
				# create an ovbject based on the ToDoItem model and saves to the record in the database

				EventItem.objects.create(event_name = event_name, description = description, date_created = timezone.now(), user_id = request.user.id)

				return redirect("eventlist:index")
			else:
				context = {
					'error' : True
				}

	return render(request, "add_event.html", context)

