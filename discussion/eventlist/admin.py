from django.contrib import admin

# Register your models here.


from .models import EventItem

admin.site.register(EventItem)