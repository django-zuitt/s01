from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.

# The from keyword allows importing of necessary classes/modules, methods and others files needed in our application from the django.http package while the import keyword defines what we are importing from the package
from django.http import HttpResponse

# Local imports
from .models import ToDoItem

# import the built in User Model
from django.contrib.auth.models import User

# to use the template created:
# from django.template import loader

# import the authenticate function
from django.contrib.auth import authenticate, login, logout

from django.forms.models import model_to_dict

from django.contrib.auth.hashers import make_password

from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm, ChangePasswordForm

from django.utils import timezone

def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
	context = {
		'todoitem_list': todoitem_list,
		"user": request.user
	}

	return render(request, "index.html", context)

def todoitem(request, todoitem_id):
	# response = f"You are viewing the details of {todoitem_id}"
	# return HttpResponse(response)

	todoitem = get_object_or_404(ToDoItem, pk = todoitem_id)

	return render(request, "todoitem.html", model_to_dict(todoitem))


# this function is responsible for registering on our application
def register(request):
	context = {}

	users = User.objects.all()
	is_user_registered = False

	# if this is a post request we need to process the form data
	if request.method == "POST":

		form = RegisterForm(request.POST)

		if form.is_valid() == False:
			form = RegisterForm()
			
		else:
			password = form.cleaned_data['password']
			confirm = form.cleaned_data['confirm']
			if password == confirm:
				username = form.cleaned_data['username']
				firstname = form.cleaned_data['firstname']
				lastname = form.cleaned_data['lastname']
				email = form.cleaned_data['email']
				is_staff = False
				is_active = True
				hashed_password = make_password(password)
				user = User.objects.create(username=username, email=email, password=hashed_password, first_name=firstname, last_name=lastname)

				for existing_user in users:
					if existing_user.username == username:
						is_user_registered = True
						break

				if is_user_registered == False:
					# to save our user
					user.save()

				if user is not None:
					context = {
						'is_user_registered': is_user_registered,
						'username' : username,
						'password' : password
					}
					login(request, user)
					return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}
	
	
	return render(request, "register.html", context)


def change_password(request):
	
	is_user_authenticated = False
	context = {}

	if request.method == "POST":

		form = ChangePasswordForm(request.POST)

		if form.is_valid() == False:
			form = ChangePasswordForm()
		else:

			username = request.user.username
			password = form.cleaned_data['changepassword']
			confirm = form.cleaned_data['changeconfirm']
			user = authenticate(username = username, password = password)

			if user is not None and password == confirm:
				authenticated_user = User.objects.get(username = username)
				authenticated_user.set_password(password)
				authenticated_user.save()

				is_user_authenticated = True
			else:
				context = {
					'error' : True
				}
			
	
			context = {
				"is_user_authenticated": is_user_authenticated
			}

	return render(request, "change_password.html", context)
			

def login_user(request):
	context = {}

	# if this is a post request we need to process the form data
	if request.method == "POST":

		form = LoginForm(request.POST)

		if form.is_valid() == False:
			form = LoginForm()
			
		else:
			print(form)
			# cleaned_data retrieves the information from the form
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			user = authenticate(username = username, password = password)

			if user is not None:
				context = {
					'username' : username,
					'password' : password
				}
				login(request, user)
				return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}
			
	
	return render(request, "login.html", context)








	# username = 'johndoe'
	# password = 'johndoe1234567'

	# user = authenticate(username = username, password = password)

	

def logout_user(request):
	logout(request)
	return redirect("todolist:index")


def add_task(request):
	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			duplicates = ToDoItem.objects.filter(task_name = task_name, user_id = request.user.id)

			if not duplicates:
				# create an ovbject based on the ToDoItem model and saves to the record in the database

				ToDoItem.objects.create(task_name = task_name, description = description, date_created = timezone.now(), user_id = request.user.id)

				return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}

	return render(request, "add_task.html", context)



def update_task(request, todoitem_id):
	todoitem = ToDoItem.objects.filter(pk = todoitem_id)

	context = {
		"user" : request.user,
		"todoitem_id": todoitem_id,
		"task_name": todoitem[0].task_name,
		"description": todoitem[0].description,
		"status": todoitem[0].status
	}

	if request.method == "POST":
		form = UpdateTaskForm(request.POST)

		if form.is_valid() == False:
			form = UpdateTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:
				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status

				todoitem[0].save()

				return redirect("todolist:viewtodoitem", todoitem_id = todoitem[0].id)

			else:
				context = {
					"error" : True
				}

	return render(request, "update_task.html", context)

def delete_task(request, todoitem_id):
	ToDoItem.objects.filter(pk = todoitem_id).delete()

	return redirect("todolist:index")