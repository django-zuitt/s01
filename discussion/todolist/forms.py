from django import forms

class RegisterForm(forms.Form):
	username = forms.CharField(label = "username", max_length = 20)
	password = forms.CharField(label = "password", max_length = 20)
	confirm = forms.CharField(label = "confirm", max_length = 20)
	firstname = forms.CharField(label = "firstname", max_length = 20)
	lastname = forms.CharField(label = "lastname", max_length = 20)
	email = forms.CharField(label = "email")

class LoginForm(forms.Form):
	username = forms.CharField(label = "username", max_length = 20)
	password = forms.CharField(label = "password", max_length = 20)

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label = 'task_name', max_length =50)
	description = forms.CharField(label = 'description', max_length = 200)

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label = 'task_name', max_length =50)
	description = forms.CharField(label = 'description', max_length = 200)
	status = forms.CharField(label = 'status', max_length = 50)

class ChangePasswordForm(forms.Form):
	changepassword = forms.CharField(label = "changepassword", max_length = 20)
	changeconfirm = forms.CharField(label = "changeconfirm", max_length = 20)