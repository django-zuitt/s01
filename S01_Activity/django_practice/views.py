from django.shortcuts import render, redirect

# Create your views here.

from django.http import HttpResponse
from .models import GroceryItem
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

def index(request):
	grocery_list = GroceryItem.objects.all()

	# template = loader.get_template("index.html")
	context = {
		'grocery_list': grocery_list
	}
	# return HttpResponse(template.render(context, request))
	return render(request, "index.html", context)

def groceryitem(request, groceryitem_id):

	groceryitem = model_to_dict(GroceryItem.objects.get(pk = groceryitem_id))

	return render(request, "groceryitem.html", groceryitem)

	# response = f"You are viewing the details of {groceryitem_id}"
	# return HttpResponse(response)

def register(request):

	users = User.objects.all()
	is_user_registered = False

	user = User()
	user.username = "loven"
	user.first_name = "loven"
	user.last_name = "manabat"
	user.email = "loven@mail.com"
	user.set_password("loven1234")
	user.is_staff = False
	user.is_active = True

	for indiv_user in users:
		if indiv_user.username == user.username:
			is_user_registered = True
			break

	if is_user_registered == False:
		user.save()

	context = {
		'is_user_registered': is_user_registered,
		'first_name': user.first_name,
		'last_name': user.last_name
	}

	return render(request, "register.html", context)

def change_password(request):
	
	is_user_authenticated = False

	user = authenticate(username = "loven", password = "loven12345")

	if user is not None:
		authenticated_user = User.objects.get(username = "loven")
		authenticated_user.set_password("loven1234")
		authenticated_user.save()

		is_user_authenticated = True

	context = {
		"is_user_authenticated": is_user_authenticated
	}

	return render(request, "change_password.html", context)

def login_user(request):
	username = 'loven'
	password = 'loven1234'

	user = authenticate(username = username, password = password)

	if user is not None:
		login(request, user)

		return redirect("index")
	else:
		return render(request, "login.html")

def logout_user(request):
	logout(request)
	return redirect("index")